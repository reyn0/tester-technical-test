﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.QualityTools.UnitTestFramework;
using OpenQA.Selenium.Interactions;

namespace UnitTestProject1
{
    public class BaseClass
    {
        private IWebDriver _driver;
        public string testUrl = ConfigurationManager.AppSettings["url"];
        private static Random random = new Random();
        ChromeOptions options = new ChromeOptions();
        

        public IWebDriver StartBrowser()
        {
            options.AddArgument("--start-maximized");
            _driver = new ChromeDriver(options);
            _driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 30));
            
            return _driver;
        }

        /// <summary>
        /// All waiting functions
        /// </summary>
        /// <param name="seconds"></param>
        #region wait
        public void wait(int seconds)
        {
            WebDriverWait nWait = new WebDriverWait(_driver, new TimeSpan(0, seconds, 0));
        }

        public void waitUntil(int seconds, string className)
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(seconds)).Until(ExpectedConditions.ElementExists(By.ClassName(className)));
        }

        public void waitUntilIDExist(int seconds, string id)
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(seconds)).Until(ExpectedConditions.ElementExists(By.Id(id)));
        }

        public void waitUntilIDClickable(int seconds,string id)
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(seconds)).Until(ExpectedConditions.ElementToBeClickable(By.Id(id)));
        }
        #endregion

        /// <summary>
        /// Quote related logic
        /// </summary>
        /// <param name="maker">Car maker</param>
        /// <param name="year">Year manufacture</param>
        /// <param name="driverAge">Driver Age</param>
        /// <param name="gender">Driver Gender</param>
        /// <param name="state">Stage</param>
        /// <param name="email">Email</param>
        #region quote
        public void getQuote(string maker, string year, string driverAge, string gender, string state, string email)
        {
            click("id", "getcarquote");
            sendText("id", "make", maker);
            sendText("id", "year", year);
            verifyElementExistByID("year");
            sendText("id", "age", driverAge);
            
            if(gender == "male")
            { 
                click("id", "male");
            }
            else if(gender == "female")
            {
                click("id", "female");
            }
            else
            {
                click("id", "male");
            }
            select("id", "state", state);
            sendText("id", "email", email);
            waitUntilIDClickable(100, "getquote");
            click("id", "getquote");
        }

        #endregion quote


        public void buyInsurance(string cardHoldername, string username, string password, string cardNumber, string expirationMonth, string expirationYear, string cardCCV)
        {
            click("id", "payment");
            sendText("id", "cardholdername", cardHoldername);
            sendText("xpath", "(//input[@id='username'])[2]", username);
            sendText("xpath", "(//input[@id='password'])[2]", password);
            sendText("xpath", "//input[@id='card-number']", cardNumber);
            sendText("id", "expiry-month", expirationMonth);
            sendText("xpath", "//select[@name='expiry-year']", expirationYear);
            sendText("id", "cvv", cardCCV);
            click("id", "pay");
        }

        /// <summary>
        /// All verification functions
        /// </summary>
        /// <param name="expected"></param>
        #region Verification
        public void verifyQuoteValue(string expected)
        {
            string actualQuote = getTextValue("class", "mark");
            verifyStringMatch(expected, actualQuote);
        }

        public void verifyLoginForm()
        {
            _driver.FindElement(By.LinkText("Login")).Click();
            Assert.IsTrue(_driver.FindElement(By.Id("loginFormUsername")).Displayed);
            Assert.IsTrue(_driver.FindElement(By.Id("password")).Displayed);
            Assert.IsTrue(_driver.FindElement(By.ClassName("submit-button")).Displayed);
        }
        #endregion

        /// <summary>
        /// Common codes like clicking, setting text and selection
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="controller"></param>
        #region Common Codes
        public void click(string identifier, string controller)
        {
            switch (identifier)
            {
                case "xpath":
                    _driver.FindElement(By.XPath(controller)).Click();
                    wait(10);
                    break;
                case "class":
                    _driver.FindElement(By.ClassName(controller)).Click();
                    wait(10);
                    break;
                case "linkText":
                    _driver.FindElement(By.LinkText(controller)).Click();
                    wait(10);
                    break;
                case "partialLinkText":
                    _driver.FindElement(By.PartialLinkText(controller)).Click();
                    wait(10);
                    break;
                default:
                    _driver.FindElement(By.Id(controller)).Click();
                    break;
            }
        }

        public void sendText(string identifier, string controller, string text)
        {
            switch (identifier)
            {
                case "xpath":
                    _driver.FindElement(By.XPath(controller)).SendKeys(text);
                    break;
                case "class":
                    _driver.FindElement(By.ClassName(controller)).SendKeys(text);
                    break;
                default:
                    _driver.FindElement(By.Id(controller)).SendKeys(text);
                    break;
            }
        }

        public void select(string identifier, string controller, string text)
        {
            switch (identifier)
            {
                case "xpath":
                    SelectElement selectXpath = new SelectElement(_driver.FindElement(By.XPath(controller)));
                    selectXpath.SelectByText(text);
                    break;
                case "class":
                    SelectElement selectClass = new SelectElement(_driver.FindElement(By.ClassName(controller)));
                    selectClass.SelectByText(text);
                    break;
                default:
                    SelectElement select = new SelectElement(_driver.FindElement(By.Id(controller)));
                    select.SelectByText(text);
                    break;
            }
        }

        public string getTextValue(string identifier, string controller)
        {
            string textValue;
            switch (identifier)
            {
                case "xpath":
                    textValue = _driver.FindElement(By.XPath(controller)).Text;
                    break;
                case "class":
                    textValue =_driver.FindElement(By.ClassName(controller)).Text;
                    break;
                default:
                    textValue = _driver.FindElement(By.Id(controller)).Text;
                    break;
            }
            return textValue;
        }

        public void goToURL(string url)
        {
            _driver.Navigate().GoToUrl(url);
        }

        public void verifyElementExistByID(string id)
        {
            Assert.IsTrue(_driver.FindElement(By.Id(id)).Displayed, "Element " + id + "is expected but not found");
        }

        public void verifyElementNotExistByID(string id)
        {
            Assert.IsFalse(_driver.FindElement(By.Id(id)).Displayed, "Element" + id + "is missing");
        }

        public void verifyElementExistByXpath(string xpath)
        {
            Assert.IsTrue(_driver.FindElement(By.XPath(xpath)).Displayed, "Element" + xpath + "is missing");
        }

        public void verifyElementExistByPartialLinkText(string linkText)
        {
            Assert.IsTrue(_driver.FindElement(By.PartialLinkText(linkText)).Displayed, "Element" + linkText + "is missing");
        }

        public void verifyTextOnPage(string expectedText)
        {
            Assert.IsNotNull(_driver.FindElement(By.XPath("//*[contains(text(), '" + expectedText + "')]")));
        }

        public void verifyTextNotOnPage(string expectedText)
        {
            Assert.IsNull(_driver.FindElement(By.XPath("//*[contains(text(), '" + expectedText + "')]")));
        }

        public void verifyCurrentURL(string expectedUrl)
        {
            Assert.IsTrue(_driver.Url.ToString().Equals(expectedUrl), "Current URL is incorrect");
        }

        public void verifyStringMatch(string expected, string actual)
        {
            Assert.AreEqual(expected, actual, "String does not match, expected is " + expected + ". But received " + actual);
        }
        #endregion

        public static string randomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
