﻿using System;
using System.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.QualityTools.UnitTestFramework;
using UnitTestProject1;

namespace AutomationFramework
{
    [TestClass]
    public class BuyCarInsuranceTest : BaseClass
    {
        public IWebDriver driver;

        [TestInitialize]
        public void InitializeSetup()
        {
            driver = StartBrowser();

            driver.Navigate().GoToUrl(testUrl);
            driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(10));
        }

        [TestCleanup]
        public void FixtureTearDown()
        {
            if (driver != null) driver.Close();
            if (driver != null) driver.Quit();
        }

        [TestMethod, TestCategory("Insurance")]
        public void BuyInsuranceTest()
        {
            getQuote("Toyota", "2010", "20", "female", "Victoria", "test@test.com");
            buyInsurance("Harry Potter", randomString(5), randomString(5), "4444333322221111", "January", "2019", "333");
            verifyTextOnPage("Internal Server Error");
        }

    }
}
