﻿using System;
using System.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.QualityTools.UnitTestFramework;


namespace UnitTestProject1
{
    [TestClass]
    public class GetQuoteTest : BaseClass
    {
        public IWebDriver driver;

        [TestInitialize]
        public void InitializeSetup()
        {
            driver = StartBrowser();

            driver.Navigate().GoToUrl(testUrl);
            driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(10));
        }

        [TestCleanup]
        public void FixtureTearDown()
        {
            if (driver != null) driver.Close();
            if (driver != null) driver.Quit();
        }

        [TestMethod, TestCategory("Quote")]
        public void GetQuote()
        {
            getQuote("Toyota", "2010", "20", "female", "Victoria", "test@test.com");
            verifyQuoteValue("$56.37");
        }

    }

}